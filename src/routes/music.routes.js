module.exports = app => {
    const music = require("../controllers/music.controller");
    var router = require("express").Router();

    router.get("/", music.getVideoInfo);

    app.use("/api/music", router);
};
