const ytdl = require("ytdl-core")

exports.getVideoInfo = async (req, res) => {
    try {
        const { videoID } = req.query
        let url = `https://www.youtube.com/watch?v=${videoID}`
        let response =  await ytdl.getInfo(url)
        res.send(response)
    } catch (error) {
        res.status(401).send({ message: error.message })
    }
};

