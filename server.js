const express = require("express");
const cors = require("cors");

const app = express();

const PORT = 8080

var corsOptions = {
    origin: `http://localhost:${PORT}`
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


require("./src/routes/music.routes")(app);

app.listen(PORT, () => {
    console.log(`[LOG]: Server is running on PORT: ${PORT}.`);
});
